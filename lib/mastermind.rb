class Code

  PEGS = {
    'R'=> 'Red',
    'G'=> 'Green',
    'B'=> 'Blue',
    'Y'=> 'Yellow',
    'O'=> 'Orange',
    'P'=> 'Purple'
  }

  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(input)
    chars = input.upcase.split('')
    raise "ArgumentError" unless chars - PEGS.keys == []
    pegs = chars.map {|ch| PEGS[ch]}
    self.new(pegs)
  end

  def self.random
    pegs = []
    4.times {|color| pegs << PEGS.values.sample}
    self.new(pegs)
  end

  def [](idx)
    @pegs[idx]
  end

  def exact_matches(other_pegs)
    matches = 0
    4.times {|idx| matches += 1 if @pegs[idx] == other_pegs.pegs[idx]}
    matches
  end

  def near_matches(other_pegs)
    matches = 0

    PEGS.values.each do |color|
      matches += [@pegs.count(color), other_pegs.pegs.count(color)].min
    end
    matches - exact_matches(other_pegs)
  end

  def ==(other_pegs)
    return false if other_pegs.class != Code
    @pegs == other_pegs.pegs
  end

  def to_s
    "[#{@pegs.join(", ")}]"
  end


end

class Game
  attr_reader :secret_code

  def initialize(code = nil)
    @secret_code = code || Code.random
  end

  def get_guess
    print 'Enter a guess (ex. "BGPR"): '
    @guess = Code.parse(gets.chomp)
  end

  def display_matches
    puts "near matches: #{@secret_code.near_matches(@guess)}"
    puts "exact matches: #{@secret_code.exact_matches(@guess)}"
  end

  def play
    10.times do |turn|
      get_guess
      break if won?
      display_matches
    end
    conclude
  end

  def won?
    @secret_code == @guess
  end

  def conclude
    if @guess = @secret_code
      puts " Congratulations! You win"
    else
      puts "Sorry, you lost..."
    end
    puts "The code was: #{@secret_code}"
  end

end

if __FILE__ == $PROGRAM_NAME
  game = Game.new
  game.play

end
